﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InfraStructure.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Web.API.Scheduler;
using Swashbuckle.AspNetCore.Swagger;
using InfraStructure;
using InfraStructure.ImportData;

namespace Web.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "ServiceScedule API", Version = "v1" });
                // Swagger 2.+ support
                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                c.AddSecurityRequirement(security);
            });
            services.AddScoped<IServiceScheduleManager, ServiceScheduleManager>();
            services.AddSingleton<ICombinedDataReader, CombinedDataReader>();
            services.AddTransient<ICustomerImport, CustomerImport>();
            services.AddTransient<ILocationImport, LocationImport>();
            services.AddTransient<IContainerImport, ContainerImport>();
            services.AddSingleton<IHostedService, ScheduleTask>();
            services.AddDbContext<FleetLinkRouteSystemContext>(options => options.UseSqlServer(Configuration.GetConnectionString("mainConnection")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Service Scedule API V1");

                c.DocumentTitle = "Title Documentation";
                c.DocExpansion(DocExpansion.None);

            });

            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InfraStructure.Models;
using InfraStructure;
using System.Diagnostics;
using Microsoft.Extensions.Logging;

namespace Web.API.Controllers
{
    [Route("api/V1.0/ServiceSchedule")]
    public class ServiceScheduleController : Controller
    {
        private readonly FleetLinkRouteSystemContext _db;
        private readonly IServiceScheduleManager _serviceScheduleManager;
        private readonly ILogger<ServiceScheduleController> _logger;


        public ServiceScheduleController(ILogger<ServiceScheduleController> logger,FleetLinkRouteSystemContext db, IServiceScheduleManager serviceScheduleManager)
        {
            _db = db;
            _serviceScheduleManager = serviceScheduleManager;
            _logger = logger;
        }
       
        [HttpGet("RouteList")]
        public List<Route> GetRouteList()
        {
            _logger.LogDebug($"GetRouteList Started");
            return _db.Route.ToList();
        }

        // GET api/values/5
        [HttpGet("RouteStops/{id}")]
        public List<RouteStop> GetRouteStops(int id)
        {
            return _serviceScheduleManager.GetRouteStops(id);
        }

        [HttpGet("GenerateServiceSchedule/{routeid}")]
        
        public IActionResult GenerateServiceSchedule(int routeid)
        {
            _logger.LogDebug($"GenerateServiceSchedule for route {routeid} Started");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            //_serviceScheduleManager.g();
            stopwatch.Stop();
            _logger.LogDebug($"service schedules are generated for Route {routeid} in {stopwatch.Elapsed.Milliseconds} Ms");

            return Ok($"service schedules are generated for Route {routeid}  in {stopwatch.Elapsed.Milliseconds} Ms");


        }

        [HttpGet("GenerateServiceScheduleAll")]

        public IActionResult GenerateServiceScheduleAll()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            _serviceScheduleManager.GenerateRouteServiceScedules();
            stopwatch.Stop();
            return Ok($"service schedules are generated for  Total Routes in {stopwatch.Elapsed.Minutes} Ms");


        }


    }
}

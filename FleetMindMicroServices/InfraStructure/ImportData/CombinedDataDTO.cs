﻿namespace InfraStructure.ImportData
{
    public class CombinedDataDTO
    {
        //Row
        public int RowNumber { get; set; }

        //Customer
        public int? CustomerId { get; set; }

        public string CustomerCode { get; set; }

        public string CustomerClass { get; set; }
        public string CustomerType { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string OfficePhone { get; set; }
        public string HomePhone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string CustomerStatus { get; set; }

        public string CustomerBillable { get; set; }
        public string CustomerCivicNumber { get; set; }
        public string CustomerStreet { get; set; }
        public string CustomerSuite { get; set; }
        public string CustomerDistrict { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerState { get; set; }
        public string CustomerPostalCode { get; set; }

        //Location
        public int LocationId { get; set; }

        public string LocationCode { get; set; }

        public string LocationCivicNumber { get; set; }
        public string LocationStreet { get; set; }
        public string LocationSuite { get; set; }
        public string LocationDistrict { get; set; }
        public string LocationCity { get; set; }
        public string LocationState { get; set; }
        public string LocationPostalCode { get; set; }
        public string BuildingCategory { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string LocationLatitude { get; set; }
        public string LocationLongitude { get; set; }
        public string LocationName { get; set; }

        public string GeozoneId { get; set; }
        public string LocationStatus { get; set; }

        //Container

        public string ContainerCode { get; set; }

        public string SerialNumber { get; set; }
        public string RFID { get; set; }
        public string Capacity { get; set; }
        public string CapacityUnits { get; set; }
        public string Color { get; set; }
        public string ContainerType { get; set; }
        public string ContainerComments { get; set; }
        public string ContainerMaker { get; set; }
        public string WarrantyStartDate { get; set; }
        public string WarrantyEndDate { get; set; }
        public string ContainerLabel { get; set; }
        public string ContainerMaterial { get; set; }
        public string ContainerDimensionUnit { get; set; }
        public string ContainerWidth { get; set; }
        public string ContainerHeight { get; set; }
        public string ContainerLength { get; set; }
        public string ContainerActive { get; set; }

        //Assignment

        public string SubscriptionCode { get; set; }

        public string AssignmentLatitude { get; set; }
        public string AssignmentLongitude { get; set; }
        public string AssignmentMaterial { get; set; }
        public string AssignmentStartDate { get; set; }
        public string AssignmentEndDate { get; set; }
        public string ServiceCode { get; set; }
        public string FacilityCode { get; set; }
        public string Rate { get; set; }

        public string BundleRateCode { get; set; }
        public string SharedContainer { get; set; }
        public string Primary { get; set; }
        public string Workflow { get; set; }
        public string TargetDate { get; set; }
        public string NoteDriver { get; set; }
        public string NoteDispatcher { get; set; }
        public string Route01 { get; set; }
        public string Sequence01 { get; set; }
        public string Route02 { get; set; }
        public string Sequence02 { get; set; }
        public string Route03 { get; set; }
        public string Sequence03 { get; set; }
        public string Route04 { get; set; }
        public string Sequence04 { get; set; }
        public string Route05 { get; set; }
        public string Sequence05 { get; set; }
        public string Route06 { get; set; }
        public string Sequence06 { get; set; }
        public string Route07 { get; set; }
        public string Sequence07 { get; set; }
        public string Route08 { get; set; }
        public string Sequence08 { get; set; }
        public string Route09 { get; set; }
        public string Sequence09 { get; set; }
        public string Route10 { get; set; }
        public string Sequence10 { get; set; }
        public string Route11 { get; set; }
        public string Sequence11 { get; set; }
        public string Route12 { get; set; }
        public string Sequence12 { get; set; }
        public string Route13 { get; set; }
        public string Sequence13 { get; set; }
        public string Route14 { get; set; }
        public string Sequence14 { get; set; }
        public string Route15 { get; set; }
        public string Sequence15 { get; set; }
        public string Route16 { get; set; }
        public string Sequence16 { get; set; }
        public string Route17 { get; set; }
        public string Sequence17 { get; set; }
        public string Route18 { get; set; }
        public string Sequence18 { get; set; }
        public string Route19 { get; set; }
        public string Sequence19 { get; set; }
        public string Route20 { get; set; }
        public string Sequence20 { get; set; }
        public string BusinessName { get; internal set; }
        public string CustomerComments { get; internal set; }
        public string ContainerLatitude { get; internal set; }
        public string ContainerLongitude { get; internal set; }
        public object ServiceMaterial { get; internal set; }
        public object SubscriptionMaterial { get; internal set; }
        public object SubscriptionStartDate { get; internal set; }
        public object SubscriptionEndDate { get; internal set; }
        public string SubscriptionStatus { get; set; }
        public string SubscriptionComments { get; set; }
        public object WordOrderId { get; internal set; }
    }
}

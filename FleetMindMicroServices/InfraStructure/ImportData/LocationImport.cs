﻿using InfraStructure.Models;
using System.Collections.Generic;
using System.Linq;

namespace InfraStructure.ImportData
{
    
    public class LocationImport:ILocationImport
    {
        private readonly FleetLinkRouteSystemContext _db;
        public LocationImport(FleetLinkRouteSystemContext db)
        {
            _db = db;
        }

        public List<CombinedDataDTO> Import(List<CombinedDataDTO> combinedDataRows)
        {

            var _locations = new List<LocationDTO>();

            foreach (var row in combinedDataRows)
            {
                var c = new LocationDTO
                {
                    Code = row.LocationCode,
                    CustomerId = row.CustomerId,
                    CivicNumber = row.LocationCivicNumber,
                    Street = row.LocationStreet,
                    Suite = row.CustomerSuite,
                    District = row.CustomerDistrict,
                    City = row.CustomerCity,
                    StateCode = row.CustomerState,
                    PostalCode = row.CustomerPostalCode,
                    RowNumber = row.RowNumber
                };

                _locations.Add(c);
            }


            
            var result = _db.LocationBulkUpsert(_locations);


            foreach (var row in combinedDataRows)
            {
                var item = result.FirstOrDefault(s => s.RowNumber == row.RowNumber);
                if (item != null)
                {
                    row.CustomerId = item.Id;
                    if (row.CustomerCode == null) row.CustomerCode = item.Code;
                }

            }

            return combinedDataRows;
        }

    }
}

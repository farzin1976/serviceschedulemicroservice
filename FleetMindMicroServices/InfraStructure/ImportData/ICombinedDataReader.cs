﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace InfraStructure.ImportData
{
    public interface ICombinedDataReader
    {
         List<CombinedDataDTO> ReadCsv(TextReader textReader);
    }
}

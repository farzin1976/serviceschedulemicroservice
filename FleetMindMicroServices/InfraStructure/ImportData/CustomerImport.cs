﻿using InfraStructure.Models;
using System.Collections.Generic;
using System.Linq;

namespace InfraStructure.ImportData
{

    public class CustomerImport:ICustomerImport
    {
        private readonly FleetLinkRouteSystemContext _db;
        public CustomerImport(FleetLinkRouteSystemContext db)
        {
            _db = db;
        }

        public List<CombinedDataDTO> Import(List<CombinedDataDTO> combinedDataRows)
        {

            var _customers = new List<CustomerDTO>();

            foreach (var row in combinedDataRows)
            {
                var c = new CustomerDTO
                {
                    Code = row.CustomerCode,
                    Class = row.CustomerClass,
                    FirstName = row.CustomerFirstName,
                    LastName = row.CustomerLastName,
                    BusinessName = row.BusinessName,
                    CivicNumber = row.CustomerCivicNumber,
                    Street = row.CustomerStreet,
                    Suite = row.CustomerSuite,
                    District = row.CustomerDistrict,
                    City = row.CustomerCity,
                    State = row.CustomerState,
                    OfficePhone = row.OfficePhone,
                    HomePhone = row.HomePhone,
                    Fax = row.Fax,
                    Email = row.Email,
                    PostalCode = row.CustomerPostalCode,
                    RowNumber = row.RowNumber
                };

                _customers.Add(c);
            }


           
            var result = _db.CustomerBulkUpsert(_customers);


            foreach (var row in combinedDataRows)
            {
                var item = result.FirstOrDefault(s => s.RowNumber == row.RowNumber);
                if (item != null)
                {
                    row.CustomerId = item.Id;
                    if (row.CustomerCode == null) row.CustomerCode = item.Code;
                }

            }

            return combinedDataRows;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure.ImportData
{
    public interface ICustomerImport
    {
        List<CombinedDataDTO> Import(List<CombinedDataDTO> combinedDataRows);
    }
}

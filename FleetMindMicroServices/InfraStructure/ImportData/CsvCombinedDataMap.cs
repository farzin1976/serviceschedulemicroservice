﻿using CsvHelper.Configuration;

namespace InfraStructure.ImportData
{
    public class CsvCombinedDataMap:ClassMap<CombinedDataDTO>
    {
        public CsvCombinedDataMap()
        {
            Map(m => m.RowNumber).ConvertUsing(row => row.Context.RawRow);
            Map(m => m.CustomerCode).Name("customer code").NameIndex(0);
            Map(m => m.CustomerType).Name("line of business");
            Map(m => m.BusinessName).Name("business name");
            Map(m => m.CustomerLastName).Name("customer last name");
            Map(m => m.CustomerFirstName).Name("customer first name");
            Map(m => m.OfficePhone).Name("office phone");
            Map(m => m.HomePhone).Name("home phone");
            Map(m => m.Fax).Name("fax");
            Map(m => m.Email).Name("email");
            Map(m => m.CustomerComments).Name("customer comments");
            //Map(m => m.CustomerBillingId).Name("customer billing id");
            Map(m => m.CustomerClass).Name("customer class");
            Map(m => m.CustomerBillable).Name("billable");

            Map(m => m.CustomerCivicNumber).Name("customer civic number");
            Map(m => m.CustomerStreet).Name("customer street");
            Map(m => m.CustomerSuite).Name("customer suite");
            Map(m => m.CustomerDistrict).Name("customer district");
            Map(m => m.CustomerCity).Name("customer city");
            Map(m => m.CustomerState).Name("customer state");
            Map(m => m.CustomerPostalCode).Name("customer postalcode");

            Map(m => m.LocationName).Name("location name");
            Map(m => m.LocationCivicNumber).Name("location civic number");
            Map(m => m.LocationStreet).Name("location street");
            Map(m => m.LocationSuite).Name("location suite");
            Map(m => m.LocationDistrict).Name("location district");
            Map(m => m.LocationCity).Name("location city");
            Map(m => m.LocationState).Name("location state");
            Map(m => m.LocationPostalCode).Name("location postalcode");
            Map(m => m.BuildingCategory).Name("building category");
            Map(m => m.ContactName).Name("contact name");
            Map(m => m.ContactPhone).Name("contact phone");
            Map(m => m.LocationLatitude).Name("location latitude");
            Map(m => m.LocationLongitude).Name("location longitude");
            Map(m => m.LocationCode).Name("location code");
            Map(m => m.GeozoneId).Name("geoid");
            Map(m => m.LocationStatus).Name("location active");

            Map(m => m.SerialNumber).Name("serial number");
            Map(m => m.RFID).Name("rfid");

            Map(m => m.Capacity).Name("capacity");
            Map(m => m.CapacityUnits).Name("capacity units");
            Map(m => m.Color).Name("color");
            Map(m => m.ContainerType).Name("container type");
            Map(m => m.ContainerComments).Name("container comments");
            Map(m => m.ContainerMaker).Name("manufacturer");
            Map(m => m.WarrantyStartDate).Name("warranty start date");
            Map(m => m.WarrantyEndDate).Name("warranty end date");
            Map(m => m.ContainerLabel).Name("label");
            Map(m => m.ContainerMaterial).Name("container material");
            Map(m => m.ContainerLongitude).Name("container longitude");
            Map(m => m.ContainerActive).Name("Container Active/Inactive");

            Map(m => m.SubscriptionMaterial).Name("subscription material");
            Map(m => m.SubscriptionStartDate).Name("subscription start date");
            Map(m => m.SubscriptionEndDate).Name("subscription end date");
            Map(m => m.SubscriptionStatus).Name("subscription active");
            Map(m => m.SubscriptionComments).Name("subscription comments");

            Map(m => m.ServiceCode).Name("service code");
            Map(m => m.FacilityCode).Name("facility code");

            Map(m => m.Rate).Name("rate discount");
            Map(m => m.SubscriptionCode).Name("subscription code");
            Map(m => m.BundleRateCode).Name("bundle rate code");
            Map(m => m.SharedContainer).Name("shared association");
            Map(m => m.Primary).Name("is primary");
            Map(m => m.Workflow).Name("workflow");

            Map(m => m.TargetDate).Name("target date");

            Map(m => m.NoteDriver).Name("driver note");
            Map(m => m.NoteDispatcher).Name("dispatcher note");
            Map(m => m.Route01).Name("route 1");
            Map(m => m.Sequence01).Name("sequence 1");
            Map(m => m.Route02).Name("route 2");
            Map(m => m.Sequence02).Name("sequence 2");

            Map(m => m.WordOrderId).Name("work order id");
        }
    }
}

﻿using InfraStructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure.ImportData
{
    public class ContainerImport : IContainerImport
    {
        private readonly FleetLinkRouteSystemContext _db;
        public ContainerImport(FleetLinkRouteSystemContext db)
        {
            _db = db;
        }
        public List<CombinedDataDTO> Import(List<CombinedDataDTO> combinedDataRows)
        {
            var _containers = new List<ContainerDTO>();
            foreach (var row in combinedDataRows)
            {
                decimal? nullableWidth = null;
                decimal dWidth = 0;
                if (decimal.TryParse(row.ContainerWidth, out dWidth))
                {
                    nullableWidth = decimal.Round(dWidth, 2);
                }

                decimal? nullableHeight = null;
                decimal dHeight = 0;

                if (decimal.TryParse(row.ContainerHeight, out dHeight))
                {
                    nullableHeight = decimal.Round(dHeight, 2);
                }

                decimal? nullableLength = null;
                decimal dLength = 0;

                if (decimal.TryParse(row.ContainerLength, out dLength))
                {
                    nullableLength = decimal.Round(dLength, 2);
                }


                var c = new ContainerDTO()
                {
                    SerialNumber = string.IsNullOrEmpty(row.SerialNumber) ? ("FRS" + Guid.NewGuid()).Replace("-", "").Substring(0, 20) : row.SerialNumber,
                    ElectronicKey = string.IsNullOrEmpty(row.RFID) ? ("FRS" + Guid.NewGuid()).Replace("-", "").Substring(0, 20) : row.RFID,
                    UnitTypeId = 1, //!string.IsNullOrEmpty(unitName) ? (int?)_referenceData.Unit[unitName.ToUpper()] : null;
                    ColorId = 1, //!string.IsNullOrEmpty(colorName) ? (int?)_referenceData.Color[colorName.ToUpper()] : null;
                    ContainerTypeId = 1, //!string.IsNullOrEmpty(contType)? (short?)_referenceData.ContainerType[contType.ToUpper()]: null;
                    ContainerMakerId=1,
                    Capacity= !string.IsNullOrEmpty(row.Capacity) ? (double?)double.Parse(row.Capacity) : null,
                    Active= !string.IsNullOrEmpty(row.ContainerActive) && row.ContainerActive.Equals("Active") ? (short)1 : (short)0,
                    WarrantyStartDate=null,
                    WarrantyEndDate=null,
                    Comments=row.ContainerComments,
                    IconName="",
                    Label=row.ContainerLabel,
                    MaterialId=1,
                    DimensionUnitId=1,
                    IsCompactor=false,
                    Width=nullableHeight,
                    Height=nullableHeight,
                    Length=nullableLength,
                    RowNumber=row.RowNumber
                };
                _containers.Add(c);
            }

             _db.ContainerBulkUpsert(_containers);

            var result = new List<CombinedDataDTO>();
            return result;
        }
    }
}

﻿using System.Collections.Generic;

namespace InfraStructure.ImportData
{
    public interface IContainerImport
    {
        List<CombinedDataDTO> Import(List<CombinedDataDTO> combinedDataRows);
    }
}

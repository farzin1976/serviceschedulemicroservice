﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace InfraStructure.ImportData
{
    public  class CombinedDataReader:ICombinedDataReader
    {
        public  List<CombinedDataDTO> ReadCsv(TextReader textReader)
        {
            var csv = new CsvHelper.CsvReader(textReader);  //CsvHelper.Configuration
            csv.Configuration.AllowComments = true;
            csv.Configuration.PrepareHeaderForMatch = header => header.ToLower();
            csv.Configuration.RegisterClassMap<CsvCombinedDataMap>();
            var records = csv.GetRecords<CombinedDataDTO>();
            return records.ToList();
        }

    }
}

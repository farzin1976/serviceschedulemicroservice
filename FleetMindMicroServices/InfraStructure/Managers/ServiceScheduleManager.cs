﻿using InfraStructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.Extensions.Logging;
namespace InfraStructure
{
    public class ServiceScheduleManager : IServiceScheduleManager
    {
        private readonly FleetLinkRouteSystemContext _db;
        private const int _serviceShedulesRecords = 10;
        private const int _serviceShedulesMaxDate = 30;
        private readonly ILogger<ServiceScheduleManager> _logger;
        public ServiceScheduleManager(FleetLinkRouteSystemContext db, ILogger<ServiceScheduleManager> logger)
        {
            _db = db;
            _logger = logger;
        }
        public List<RouteStop> GetRouteStops(int routeId)
        {
            return _db.GetRouteStops(routeId);
        }
        public void GenerateRouteServiceScedules()
        {
            List<ServiceSchedule> serviceSchedules=new List<ServiceSchedule>();
            var routes = _db.Route;
            foreach (var route in routes)
            {
                serviceSchedules.Clear();
                _logger.LogDebug($"service schedules are generated for Route {route.AutoId}");
                var routeStops = GetRouteStops(route.AutoId);
                foreach (var item in routeStops)
                {
                    serviceSchedules.AddRange(PopulateServiceSchedule(item.ServiceId, item.AsnId, item.LastRouteDsipatchedDate, item.NextRouteScheduleDate, item.NextDispatchDate, item.AsnStartDate.Value, item.AsnEndDate));
                    if (serviceSchedules.Count == 5000)
                    {
                        _db.ServiceSchedule.AddRange(serviceSchedules);
                        _db.SaveChanges();
                        serviceSchedules.Clear();
                    }
                }

                //Insert generated serviceSchedules

                _db.ServiceSchedule.AddRange(serviceSchedules);
                _db.SaveChanges();

            }


        }

        public IEnumerable<ServiceSchedule> PopulateServiceSchedule(int serviceId,
                                              int assignmentId,
                                              DateTime? lastRouteDispatchDate,
                                              DateTime? nextRouteScheduleDate,
                                              DateTime? nextServiceScheduledDate,
                                              DateTime serviceStartDate,
                                              DateTime? serviceEndDate)
        {

            var newServiceSchedules = Generate(serviceId,
                                               assignmentId,
                                               lastRouteDispatchDate,
                                               nextRouteScheduleDate,
                                               nextServiceScheduledDate,
                                               serviceStartDate,
                                               serviceEndDate);

            if (newServiceSchedules.Any())
            {

                
                var OldServiceSchedules = _db.ServiceSchedule.Where(c=>c.ServiceId==serviceId).Where(c=>c.AssignmentId==assignmentId);
                //Delete Old serviceSchedules
                foreach (var OldServiceSchedule in OldServiceSchedules)
                {
                    if (OldServiceSchedule.ScheduleStatus != 9)
                    {
                        _db.ServiceSchedule.Remove(OldServiceSchedule);

                    }
                }
                _db.SaveChanges();
                //Insert generated serviceSchedules
               
                //_db.ServiceSchedule.AddRange(newServiceSchedules);
                //_db.SaveChanges();
            }
            return newServiceSchedules;

        }

        internal IEnumerable<ServiceSchedule> Generate(int serviceId,
                                                                    int assignmentId,
                                                                    DateTime? lastRouteDispatchDate,
                                                                    DateTime? nextRouteScheduleDate,
                                                                    DateTime? nextServiceScheduledDate,
                                                                    DateTime serviceStartDate,
                                                                    DateTime? serviceEndDate)
        {
           
            List<ServiceSchedule> returnServiceSchedules = new List<ServiceSchedule>();
            DateTime date = DateTime.Now;
            var serviceSchedule = new ServiceSchedule
            {
                ServiceId = serviceId,
                AssignmentId = assignmentId,
            };
            var serviceShedules = _db.ServiceSchedule.Where(c => c.ServiceId == serviceId).Where(c => c.AssignmentId == assignmentId);
            if (serviceShedules.Any())
            {
                
                //regenerate serviceSchedules
                DateTime? routeRefrenceDate = lastRouteDispatchDate.HasValue ? lastRouteDispatchDate : nextRouteScheduleDate;
                if (routeRefrenceDate.HasValue && nextServiceScheduledDate.HasValue && routeRefrenceDate.Value >= nextServiceScheduledDate.Value)
                {

                    ServiceSchedule nextServiceScheduled;

                    //ServiceSchedules need to be regenrated
                    DateTime refrenceDate = ReturnRefrenceDate(lastRouteDispatchDate, nextRouteScheduleDate);
                    while (nextServiceScheduledDate.Value <= refrenceDate)
                    {
                        nextServiceScheduled = Calculate(serviceId, assignmentId, nextServiceScheduledDate.Value, false);
                        nextServiceScheduledDate = nextServiceScheduled.ScheduledDate;

                    }

                    FillServiceSchedules(returnServiceSchedules, serviceId, assignmentId, nextServiceScheduledDate.Value, serviceEndDate);
                }

            }
            else
            {
                //generate new serviceSchedules
                DateTime refrenceDate = ReturnRefrenceDate(lastRouteDispatchDate, nextRouteScheduleDate);
                if (refrenceDate > serviceStartDate)
                    date = refrenceDate;
                else
                    date = serviceStartDate;

                FillServiceSchedules(returnServiceSchedules, serviceId, assignmentId, date, serviceEndDate);



            }
            return returnServiceSchedules;
        }


        #region Privates
        private void FillServiceSchedules(List<ServiceSchedule> retServiceSchedules, int serviceId, int assignmentId, DateTime date, DateTime? serviceEndDate)
        {
            int counter = 0;
            var serviceSchedule = Calculate(serviceId, assignmentId, date, true);
            DateTime endDate = serviceEndDate.HasValue ? serviceEndDate.Value : DateTime.MaxValue;
            if (serviceSchedule.ScheduledDate.Value < endDate)
            {
                retServiceSchedules.Add(serviceSchedule);


                while (counter < _serviceShedulesRecords)
                {
                    counter++;
                    serviceSchedule = Calculate(serviceId, assignmentId, serviceSchedule.ScheduledDate.Value, false);

                    if (serviceSchedule.ScheduledDate.Value >= endDate || serviceSchedule.ScheduledDate.Value > date.AddDays(_serviceShedulesMaxDate))
                        break;

                    retServiceSchedules.Add(serviceSchedule);


                }
            }
        }

        private DateTime ReturnRefrenceDate(DateTime? lastRouteDispatchDate, DateTime? nextRouteScheduleDate)
        {
            DateTime refrenceDate = DateTime.Now;
            if (lastRouteDispatchDate.HasValue)
            {
                refrenceDate = lastRouteDispatchDate.Value;
            }
            else if (nextRouteScheduleDate.HasValue)
            {
                refrenceDate = nextRouteScheduleDate.Value;
            }
            return refrenceDate;
        }

        private ServiceSchedule Calculate(int serviceId, int assignmentId, DateTime date, bool first)
        {
            var service = _db.ServiceType.Find(serviceId);
            var serviceFrequency = _db.Frequency.Find(service.ServiceFrequencyId);
            var baseFrequency = BaseFrequencies.Instance.Values.FirstOrDefault(s => s.Id == serviceFrequency.BaseFrequencyId);
            var baseIntervalUnit = baseFrequency.DateIntervalUnit * (first ? 0 : 1);
            var serviceSchedule = new ServiceSchedule
            {
                ServiceId = serviceId,
                AssignmentId = assignmentId,
                ScheduleStatus = first ? 0 : 1

            };
            DateTime scheduledDate = date;
            var frequencyInterval = serviceFrequency.FrequencyInterval == null ? 1 : (int)serviceFrequency.FrequencyInterval;
            if (baseFrequency.DateInterval == "d") scheduledDate = date.AddDays(baseIntervalUnit * frequencyInterval);
            if (baseFrequency.DateInterval == "m")
            {
                if (serviceFrequency.WeekOfMonth == null)
                {
                    scheduledDate = date.AddMonths(baseIntervalUnit * frequencyInterval);
                }
                else
                {
                    int weekOfMonth = (date.Day + ((int)date.DayOfWeek)) / 7;
                    if (weekOfMonth == serviceFrequency.WeekOfMonth)
                    {
                        scheduledDate = date;
                    }
                    else
                    {
                        scheduledDate = date.AddDays(7 * (((int)serviceFrequency.WeekOfMonth) - weekOfMonth));
                    }
                }
            }
            if (baseFrequency.DateInterval == "w")
            {
                scheduledDate = date.AddDays(7 * frequencyInterval * baseIntervalUnit);
            }
            serviceSchedule.ScheduledDate = scheduledDate.AddDays(((int)scheduledDate.DayOfWeek) * -1);

            return serviceSchedule;

        }

        #endregion
    }
}

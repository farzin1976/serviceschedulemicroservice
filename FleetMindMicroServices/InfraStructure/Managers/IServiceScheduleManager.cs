﻿using InfraStructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure
{
    public interface IServiceScheduleManager
    {
        List<RouteStop> GetRouteStops(int routeId);
        IEnumerable<ServiceSchedule> PopulateServiceSchedule(int serviceId,
                                            int assignmentId,
                                            DateTime? lastRouteDispatchDate,
                                            DateTime? nextRouteScheduleDate,
                                            DateTime? nextServiceScheduledDate,
                                            DateTime serviceStartDate,
                                            DateTime? serviceEndDate);
        void GenerateRouteServiceScedules();
    }
}

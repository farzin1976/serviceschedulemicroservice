﻿using System;


namespace InfraStructure
{
    public class BaseFrequency
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string DateInterval { get; set; }
        public int DateIntervalUnit { get; set; }
        public DateTime CreatedTs { get; set; }
        public DateTime? ModifiedTs { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}

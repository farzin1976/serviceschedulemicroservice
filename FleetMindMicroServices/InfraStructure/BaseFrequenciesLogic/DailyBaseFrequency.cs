﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure
{
    public sealed class DailyBaseFrequency
    {
        public const int Id = 5;
        public const string Code = "D";
        public const string Description = "Daily";
        public const string DateInterval = "d";
        public const int DateIntervalUnit = 1;

        private DailyBaseFrequency() { }

        public static BaseFrequency Create()
        {
            return new BaseFrequency
            {
                Id = Id,
                Code = Code,
                Description = Description,
                DateInterval = DateInterval,
                DateIntervalUnit = DateIntervalUnit,
                CreatedTs = DateTime.Parse("2018-06-01"),
                CreatedBy = "System"
            };
        }
    }
}

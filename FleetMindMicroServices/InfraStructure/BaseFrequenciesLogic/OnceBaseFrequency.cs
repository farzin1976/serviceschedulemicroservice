﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure
{
    public sealed class OnceBaseFrequency
    {
        public const int Id = 0;
        public const string Code = "C";
        public const string Description = "Once";
        public const string DateInterval = "once";
        public const int DateIntervalUnit = 0;

        private OnceBaseFrequency() { }

        public static BaseFrequency Create()
        {
            return new BaseFrequency
            {
                Id = Id,
                Code = Code,
                Description = Description,
                DateInterval = DateInterval,
                DateIntervalUnit = DateIntervalUnit,
                CreatedTs = DateTime.Parse("2018-06-01"),
                CreatedBy = "System"
            };
        }
    }
}

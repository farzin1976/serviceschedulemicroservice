﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure
{
    public sealed class OddWeekBaseFrequency
    {
        public const int Id = 99;
        public const string Code = "Odd";
        public const string Description = "Odd Week";
        public const string DateInterval = "w";
        public const int DateIntervalUnit = 2;

        private OddWeekBaseFrequency() { }

        public static BaseFrequency Create()
        {
            return new BaseFrequency
            {
                Id = Id,
                Code = Code,
                Description = Description,
                DateInterval = DateInterval,
                DateIntervalUnit = DateIntervalUnit,
                CreatedTs = DateTime.Parse("2018-06-01"),
                CreatedBy = "System"
            };
        }
    }
}

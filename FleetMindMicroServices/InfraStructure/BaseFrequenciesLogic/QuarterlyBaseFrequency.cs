﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure
{
    public sealed class QuarterlyBaseFrequency
    {
        public const int Id = 2;
        public const string Code = "Q";
        public const string Description = "Quarterly";
        public const string DateInterval = "q";
        public const int DateIntervalUnit = 1;

        private QuarterlyBaseFrequency() { }

        public static BaseFrequency Create()
        {
            return new BaseFrequency
            {
                Id = Id,
                Code = Code,
                Description = Description,
                DateInterval = DateInterval,
                DateIntervalUnit = DateIntervalUnit,
                CreatedTs = DateTime.Parse("2018-06-01"),
                CreatedBy = "System"
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure
{
    public sealed class WeeklyBaseFrequency
    {
        public const int Id = 4;
        public const string Code = "W";
        public const string Description = "Weekly";
        public const string DateInterval = "w";
        public const int DateIntervalUnit = 1;

        private WeeklyBaseFrequency() { }

        public static BaseFrequency Create()
        {
            return new BaseFrequency
            {
                Id = Id,
                Code = Code,
                Description = Description,
                DateInterval = DateInterval,
                DateIntervalUnit = DateIntervalUnit,
                CreatedTs = DateTime.Parse("2018-06-01"),
                CreatedBy = "System"
            };
        }
    }
}

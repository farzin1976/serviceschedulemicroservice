﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure
{
    public sealed class AnnualBaseFrequency
    {
        public const int Id = 1;
        public const string Code = "A";
        public const string Description = "Annual";
        public const string DateInterval = "y";
        public const int DateIntervalUnit = 1;

        private AnnualBaseFrequency() { }

        public static BaseFrequency Create()
        {
            return new BaseFrequency
            {
                Id = Id,
                Code = Code,
                Description = Description,
                DateInterval = DateInterval,
                DateIntervalUnit = DateIntervalUnit,
                CreatedTs = DateTime.Parse("2018-06-01"),
                CreatedBy = "System"
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure
{
    public sealed class EvenWeekBaseFrequency
    {
        public const int Id = 88;
        public const string Code = "Even";
        public const string Description = "Even Week";
        public const string DateInterval = "w";
        public const int DateIntervalUnit = 2;

        private EvenWeekBaseFrequency() { }

        public static BaseFrequency Create()
        {
            return new BaseFrequency
            {
                Id = Id,
                Code = Code,
                Description = Description,
                DateInterval = DateInterval,
                DateIntervalUnit = DateIntervalUnit,
                CreatedTs = DateTime.Parse("2018-06-01"),
                CreatedBy = "System"
            };
        }
    }
}

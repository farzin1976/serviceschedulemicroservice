﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure
{
    public sealed class BaseFrequencies
    {
        private static BaseFrequencies instance = null;
        private static readonly object padlock = new object();
        private static readonly List<BaseFrequency> values = new List<BaseFrequency>();

        BaseFrequencies()
        {
            values.Add(OnceBaseFrequency.Create());
            values.Add(AnnualBaseFrequency.Create());
            values.Add(QuarterlyBaseFrequency.Create());
            values.Add(MonthlyBaseFrequency.Create());
            values.Add(WeeklyBaseFrequency.Create());
            values.Add(DailyBaseFrequency.Create());
            values.Add(EvenWeekBaseFrequency.Create());
            values.Add(OddWeekBaseFrequency.Create());
        }

        public static BaseFrequencies Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new BaseFrequencies();
                    }
                    return instance;
                }
            }
        }

        public IEnumerable<BaseFrequency> Values
        {
            get
            {
                return values;
            }
        }

    }
}

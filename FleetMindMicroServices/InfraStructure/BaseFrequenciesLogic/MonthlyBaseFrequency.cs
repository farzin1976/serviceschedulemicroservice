﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure
{
    public sealed class MonthlyBaseFrequency
    {
        public const int Id = 3;
        public const string Code = "M";
        public const string Description = "Monthly";
        public const string DateInterval = "m";
        public const int DateIntervalUnit = 1;

        private MonthlyBaseFrequency() { }

        public static BaseFrequency Create()
        {
            return new BaseFrequency
            {
                Id = Id,
                Code = Code,
                Description = Description,
                DateInterval = DateInterval,
                DateIntervalUnit = DateIntervalUnit,
                CreatedTs = DateTime.Parse("2018-06-01"),
                CreatedBy = "System"
            };
        }
    }
}

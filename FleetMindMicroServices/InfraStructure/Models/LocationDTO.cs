﻿namespace InfraStructure.Models
{
    public class LocationDTO:Location
    {
        public int RowNumber { get; set; }
    }
}

﻿namespace InfraStructure.Models
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class RouteStop
    {
        public RouteStop() { }

        [DataMember(Name = "stopId")]

        public int StopId { get; set; }

        [DataMember(Name = "transferred")]
        public string Transferred { get; set; }

        [DataMember(Name = "routeId")]
        public int RouteId { get; set; }

        [DataMember(Name = "routeName")]
        public string RouteName { get; set; }

        [DataMember(Name = "seqNumber")]
        public Int16 SeqNumber { get; set; }

        [DataMember(Name = "locationId")]
        public int LocationId { get; set; }

        [DataMember(Name = "address")]
        public string Address { get; set; }

        [DataMember(Name = "locLatitude")]
        public string LocLatitude { get; set; }

        [DataMember(Name = "locLongitude")]
        public string LocLongitude { get; set; }

        [DataMember(Name = "customerAutoId")]
        public int CustomerAutoId { get; set; }

        [DataMember(Name = "businessName")]
        public string BusinessName { get; set; }

        [DataMember(Name = "customerId")]
        public string CustomerId { get; set; }

        [DataMember(Name = "asnId")]
        public int AsnId { get; set; }

        [DataMember(Name = "statusDate")]
        public DateTime? StatusDate { get; set; }

        [DataMember(Name = "asnStartDate")]
        public DateTime? AsnStartDate { get; set; }

        [DataMember(Name = "asnEndDate")]
        public DateTime? AsnEndDate { get; set; }

        [DataMember(Name = "pickupLat")]
        public string PickupLat { get; set; }

        [DataMember(Name = "pickupLon")]
        public string PickupLon { get; set; }

        [DataMember(Name = "lobName")]
        public string LobName { get; set; }

        [DataMember(Name = "lobDesc")]
        public string LobDesc { get; set; }

        [DataMember(Name = "contId")]
        public int ContId { get; set; }

        [DataMember(Name = "serialNumber")]
        public string SerialNumber { get; set; }

        [DataMember(Name = "rfid")]
        public string Rfid { get; set; }

        [DataMember(Name = "contColorId")]
        public int ContColorId { get; set; }

        [DataMember(Name = "contColor")]
        public string ContColor { get; set; }

        [DataMember(Name = "contIcon")]
        public string ContIcon { get; set; }

        [DataMember(Name = "serviceCode")]
        public string ServiceCode { get; set; }

        [DataMember(Name = "serviceDesc")]
        public string ServiceDesc { get; set; }

        [DataMember(Name = "contCapacity")]
        public double? ContCapacity { get; set; }

        [DataMember(Name = "asnStatusCode")]
        public string AsnStatusCode { get; set; }

        [DataMember(Name = "workFlowDesc")]
        public string WorkFlowDesc { get; set; }

        [DataMember(Name = "workFlowCode")]
        public string WorkFlowCode { get; set; }

        [DataMember(Name = "nextDispatchDate")]
        public DateTime? NextDispatchDate { get; set; }

        [DataMember(Name = "nextRouteScheduleDate")]
        public DateTime? NextRouteScheduleDate { get; set; }

        [DataMember(Name = "lastRouteDsipatchedDate")]
        public DateTime? LastRouteDsipatchedDate { get; set; }
        [DataMember(Name = "serviceId")]
        public int ServiceId { get; set; }
    }
}

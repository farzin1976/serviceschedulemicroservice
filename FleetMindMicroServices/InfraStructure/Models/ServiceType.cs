﻿using System;
using System.Collections.Generic;

namespace InfraStructure.Models
{
    public partial class ServiceType
    {
        public ServiceType()
        {
            ServiceSchedule = new HashSet<ServiceSchedule>();
        }

        public int AutoId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? CustomerTypeAutoId { get; set; }
        public int? MaterialTypeId { get; set; }
        public double? Weight { get; set; }
        public int? WeightUnitId { get; set; }
        public bool? OnDemand { get; set; }
        public bool? Extra { get; set; }
        public short? Sequence { get; set; }
        public int? DispatchTypeAutoId { get; set; }
        public int? DefaultWorkFlowAutoId { get; set; }
        public bool? Recurring { get; set; }
        public DateTime? ModifiedTs { get; set; }
        public int? ServiceRateFrequencyAutoId { get; set; }
        public decimal? MinWeight { get; set; }
        public bool IsAlley { get; set; }
        public DateTime CreatedTs { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public int? ServiceFrequencyId { get; set; }

        public ICollection<ServiceSchedule> ServiceSchedule { get; set; }
    }
}

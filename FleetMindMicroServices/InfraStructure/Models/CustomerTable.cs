﻿using System.Collections.Generic;
using System.Data;


namespace InfraStructure.Models
{
    public static class CustomerTable
    {
        public static DataTable Create(IEnumerable<CustomerDTO> customers)
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("Code", typeof(string)));
            table.Columns.Add(new DataColumn("BusinessName", typeof(string)));
            table.Columns.Add(new DataColumn("LastName", typeof(string)));
            table.Columns.Add(new DataColumn("FirstName", typeof(string)));
            table.Columns.Add(new DataColumn("Officephone", typeof(string)));
            table.Columns.Add(new DataColumn("Homephone", typeof(string)));
            table.Columns.Add(new DataColumn("Fax", typeof(string)));
            table.Columns.Add(new DataColumn("Email", typeof(string)));
            table.Columns.Add(new DataColumn("BillingId", typeof(string)));
            table.Columns.Add(new DataColumn("CivicNumber", typeof(string)));
            table.Columns.Add(new DataColumn("Street", typeof(string)));
            table.Columns.Add(new DataColumn("Suite", typeof(string)));
            table.Columns.Add(new DataColumn("District", typeof(string)));
            table.Columns.Add(new DataColumn("City", typeof(string)));
            table.Columns.Add(new DataColumn("State", typeof(string)));
            table.Columns.Add(new DataColumn("PostalCode", typeof(string)));
            table.Columns.Add(new DataColumn("Active", typeof(short)));
            table.Columns.Add(new DataColumn("Billable", typeof(bool)));
            table.Columns.Add(new DataColumn("Comments", typeof(string)));
            table.Columns.Add(new DataColumn("ClassId", typeof(string)));
            table.Columns.Add(new DataColumn("CustomerPrfxId", typeof(string)));
            table.Columns.Add(new DataColumn("User", typeof(string)));
            table.Columns.Add(new DataColumn("SuspensionStartTs", typeof(string)));
            table.Columns.Add(new DataColumn("SuspensionEndTs", typeof(string)));
            table.Columns.Add(new DataColumn("RowNumber", typeof(int)));

            foreach (var c in customers)
            {
                DataRow row = table.NewRow();
                row["Code"] = c.Code;
                row["BusinessName"] = c.BusinessName;
                row["LastName"] = c.LastName.Trim();
                row["FirstName"] = c.FirstName;
                row["OfficePhone"] = c.OfficePhone;
                row["HomePhone"] = c.HomePhone;
                row["Fax"] = c.Fax;
                row["Email"] = c.Email;
                row["BillingId"] = "";
                row["CivicNumber"] = c.CivicNumber;
                row["Street"] = c.Street.Trim();
                row["Suite"] = "";
                row["District"] = "";
                row["City"] = c.City;
                row["State"] = c.State.Trim();
                row["PostalCode"] = c.PostalCode.Trim();
                row["Active"] = 0;
                row["Billable"] = 0;
                row["Comments"] = c.Comments;
                row["RowNumber"] = c.RowNumber;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}

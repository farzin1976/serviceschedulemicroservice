﻿using System;
using System.Collections.Generic;

namespace InfraStructure.Models
{
    public partial class ServiceSchedule
    {
        public int AutoId { get; set; }
        public DateTime? ScheduledDate { get; set; }
        public int ScheduleStatus { get; set; }
        public int ServiceId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedTs { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedTs { get; set; }
        public DateTime? ServiceDate { get; set; }
        public int AssignmentId { get; set; }

        public ServiceType Service { get; set; }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace InfraStructure.Models
{
    [DataContract]
    public class Container 
    {

        public Container()
        {
            //this.assignments = new HashSet<assignment>();
            //this.customerComplaints = new HashSet<customerComplaint>();
            //this.yardContainers = new HashSet<yardContainer>();
        }

        [DataMember(Name = "autoId")]
        public int Id { get; set; }
        [DataMember(Name = "serialNumber")]
        public string SerialNumber { get; set; }
        [DataMember(Name = "version")]
        public short? Version { get; set; }
        [DataMember(Name = "description")]
        public string Description { get; set; }
        [DataMember(Name = "electronicKey")]
        public string ElectronicKey { get; set; }
        [DataMember(Name = "capacity")]
        public double? Capacity { get; set; }
        public string UnitType { get; set; }
        [DataMember(Name = "unit")]
        public int? UnitTypeId { get; set; }
        [DataMember(Name = "colour")]
        public int? ColorId { get; set; }
        public string Color { get; set; }
        [DataMember(Name = "containerTypeId")]
        public int? ContainerTypeId { get; set; }
        public string ContainerType { get; set; }
        [DataMember(Name = "active")]
        public short? Active { get; set; }
        [DataMember(Name = "damaged")]
        public bool? Damaged { get; set; }
        [DataMember(Name = "comments")]
        public string Comments { get; set; }
        [DataMember(Name = "iconName")]
        public string IconName { get; set; }
        [DataMember(Name = "containerMakerId")]
        public int? ContainerMakerId { get; set; }
        public string ContainerMaker { get; set; }
        [DataMember(Name = "warrantyStartDate")]
        public DateTime? WarrantyStartDate { get; set; }
        [DataMember(Name = "warrantyEndDate")]
        public DateTime? WarrantyEndDate { get; set; }
        [DataMember(Name = "label")]
        public string Label { get; set; }
        [DataMember(Name = "isCompactor")]
        public bool? IsCompactor { get; set; }
        [DataMember(Name = "width")]
        public decimal? Width { get; set; }
        [DataMember(Name = "dimensionUnit")]
        public int? DimensionUnitId { get; set; }
        public string DimensionUnit { get; set; }
        [DataMember(Name = "length")]
        public decimal? Length { get; set; }
        [DataMember(Name = "height")]
        public decimal? Height { get; set; }
        [DataMember(Name = "materialId")]
        public int? MaterialId { get; set; }
        public string Material { get; set; }

        //[DataMember(Name = "yardHistory")]
        //public IList YardHistory { get; set; }

        //[DataMember(Name = "activeYard")]
        //public int? ActiveYard { get; set; }


        //[DataMember(Name = "linkedLocations")]
        //public IList LinkedLocations { get; set; }

        //public virtual ICollection<Assignment> Assignments { get; set; }
        //public virtual colour colour1 { get; set; }
        //public virtual ICollection<customerComplaint> customerComplaints { get; set; }
        //public virtual containerType containerType { get; set; }
        //public virtual containerMaker containerMaker { get; set; }
        //public virtual unitType unitType { get; set; }
        //public virtual material material { get; set; }
        //public virtual unitType unitType1 { get; set; }
        //public virtual ICollection<YardContainer> YardContainers { get; set; }
    }
}

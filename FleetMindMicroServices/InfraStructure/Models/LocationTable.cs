﻿using System.Collections.Generic;
using System.Data;

namespace InfraStructure.Models
{
    public static class LocationTable
    {
        public static DataTable Create(IEnumerable<LocationDTO> locations)
        {
            var table = new DataTable();
            table.Columns.Add(new DataColumn("Code", typeof(string)));
            table.Columns.Add(new DataColumn("CivicNumber", typeof(string)));
            table.Columns.Add(new DataColumn("Street", typeof(string)));
            table.Columns.Add(new DataColumn("Suite", typeof(string)));
            table.Columns.Add(new DataColumn("District", typeof(string)));
            table.Columns.Add(new DataColumn("City", typeof(string)));
            table.Columns.Add(new DataColumn("State", typeof(string)));
            table.Columns.Add(new DataColumn("PostalCode", typeof(string)));
            var buildingCategoryId = new DataColumn("BuildingCategoryId", typeof(int));
            buildingCategoryId.AllowDBNull = true;
            table.Columns.Add(buildingCategoryId);
            table.Columns.Add(new DataColumn("CustomerId", typeof(string)));
            table.Columns.Add(new DataColumn("ContactName", typeof(string)));
            table.Columns.Add(new DataColumn("ContactPhone", typeof(string)));
            table.Columns.Add(new DataColumn("Latitude", typeof(string)));
            table.Columns.Add(new DataColumn("Longitude", typeof(string)));
            table.Columns.Add(new DataColumn("LocationName", typeof(string)));
            //table.Columns.Add(new DataColumn("BillingId", typeof(string)));
            table.Columns.Add(new DataColumn("GeoZoneId", typeof(int)));
            table.Columns.Add(new DataColumn("User", typeof(string)));
            table.Columns.Add(new DataColumn("RowNumber", typeof(int)));


            foreach (var l in locations)
            {
                var row = table.NewRow();
                row["Code"] = l.Code;
                row["CivicNumber"] = l.CivicNumber;
                row["Street"] = l.Street;
                row["Suite"] = l.Suite;
                row["District"] = l.District;
                row["City"] = l.City.Trim();
                row["State"] = l.StateCode.Trim();
                row["PostalCode"] = l.PostalCode.Trim();
                if (l.BuildingCategoryId != null) row["BuildingCategoryId"] = l.BuildingCategoryId;
                row["CustomerId"] = l.CustomerId;
                row["Latitude"] = l.Latitude;
                row["Longitude"] = l.Longitude;
                row["LocationName"] = l.LocationName;
                if (l.GeoId != null) row["GeoZoneId"] = l.GeoId;
                row["RowNumber"] = l.RowNumber;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}

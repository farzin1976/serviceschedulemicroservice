﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraStructure.Models
{
    public class ContainerBulkUpsert
    {
        public int Id { get; set; }
        public string SerialNumber { get; set; }
        public int RowNumber { get; set; }
    }
}

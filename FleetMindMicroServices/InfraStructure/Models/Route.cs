﻿using System;
using System.Collections.Generic;

namespace InfraStructure.Models
{
    public partial class Route
    {
        public Route()
        {
            RouteSchedule = new HashSet<RouteSchedule>();
        }

        public int AutoId { get; set; }
        public string Name { get; set; }
        public int? ContainerTypeId1 { get; set; }
        public int? ContainerTypeId2 { get; set; }
        public int? ContainerTypeId3 { get; set; }
        public int? MaterialAutoId1 { get; set; }
        public int? MaterialAutoId2 { get; set; }
        public int? MaterialAutoId3 { get; set; }
        public int? OperatorAutoId { get; set; }
        public int? TruckAutoId { get; set; }
        public int? FrequencyAutoId { get; set; }
        public DateTime? ServiceDate { get; set; }
        public DateTime? LastModified { get; set; }
        public int? CustomerTypeId { get; set; }
        public bool? IsTemporary { get; set; }
        public string Status { get; set; }
        public bool? UpdateGeoLocation { get; set; }
        public bool? UpdateRfid { get; set; }
        public bool? UpdateSerNbr { get; set; }
        public short? WorkGroupAutoId { get; set; }
        public int? WorkFlowGroupAutoId { get; set; }
        public string Message { get; set; }
        public int? PermTruckAutoId { get; set; }
        public int? TmpTruckAutoId { get; set; }
        public DateTime? TmpTruckStartDate { get; set; }
        public DateTime? TmpTruckEndDate { get; set; }
        public DateTime? LastDispatchTs { get; set; }
        public bool MultiDayRoute { get; set; }
        public int? ExpirationDays { get; set; }
        public DateTime CreatedTs { get; set; }
        public DateTime? ModifiedTs { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public Frequency FrequencyAuto { get; set; }
        public ICollection<RouteSchedule> RouteSchedule { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.Data;
using System;
using System.Reflection;
using System.Linq;
using System.Runtime.CompilerServices;

namespace InfraStructure.Models
{
    public static class DataTableHelper
    {
        public static void SetDataTableRow<T>(this DataRow row,string rowName, T input, bool hasvalue)
        {
            FieldInfo[] basefields = input.GetType().BaseType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
            FieldInfo[] childfields = input.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
            FieldInfo field = basefields.Concat(childfields).ToArray().Where(c=>c.Name.ToLower()==$"<{rowName.ToLower()}>k__backingfield").FirstOrDefault();
            
            if (hasvalue && field.GetValue(input) == null)
                row[rowName] = DBNull.Value;
            else
                row[rowName] = field.GetValue(input);
        }
    }
    public static class ContainerTable
    {
        public static DataTable Create(IEnumerable<ContainerDTO> customers)
        {
            DataTable table = new DataTable();
            //table.Columns.Add(new DataColumn("Code", typeof(string)));
            table.Columns.Add(new DataColumn("serialNumber", typeof(string)));
            table.Columns.Add(new DataColumn("version", typeof(short)));
            table.Columns.Add(new DataColumn("description", typeof(string)));
            table.Columns.Add(new DataColumn("electronicKey", typeof(string)));
            table.Columns.Add(new DataColumn("capacity", typeof(double)));
            table.Columns.Add(new DataColumn("unit", typeof(int)));
            table.Columns.Add(new DataColumn("colour", typeof(int)));
            table.Columns.Add(new DataColumn("containerTypeId", typeof(int)));
            table.Columns.Add(new DataColumn("active", typeof(short)));
            table.Columns.Add(new DataColumn("damaged", typeof(bool)));
            table.Columns.Add(new DataColumn("comments", typeof(string)));
            table.Columns.Add(new DataColumn("iconName", typeof(string)));
            table.Columns.Add(new DataColumn("containerMakerId", typeof(int)));
            table.Columns.Add(new DataColumn("warrantyStartDate", typeof(DateTime)));
            table.Columns.Add(new DataColumn("warrantyEndDate", typeof(DateTime)));
            table.Columns.Add(new DataColumn("label", typeof(string)));
            table.Columns.Add(new DataColumn("isCompactor", typeof(bool)));
            table.Columns.Add(new DataColumn("dimensionUnit", typeof(int)));
            table.Columns.Add(new DataColumn("width", typeof(double)));
            table.Columns.Add(new DataColumn("length", typeof(double)));
            table.Columns.Add(new DataColumn("height", typeof(double)));
            table.Columns.Add(new DataColumn("materialId", typeof(int)));
            table.Columns.Add(new DataColumn("RowNumber", typeof(int)));

            foreach (var c in customers)
            {
                DataRow row = table.NewRow();
                row.SetDataTableRow("serialNumber", c,false);
                row.SetDataTableRow("version", c, true);
                row.SetDataTableRow("description", c, false);
                row.SetDataTableRow("electronicKey", c, false);
                row.SetDataTableRow("capacity", c, false);
                //row.SetDataTableRow("unit", c, false);
                //row.SetDataTableRow("colour", c, false);
                row.SetDataTableRow("containerTypeId", c, false);
                row.SetDataTableRow("active", c, false);
                row.SetDataTableRow("active", c, false);
                row.SetDataTableRow("damaged", c, false);
                row.SetDataTableRow("comments", c, false);
                row.SetDataTableRow("iconName", c, false);
                row.SetDataTableRow("containerMakerId", c, false);
                row.SetDataTableRow("warrantyStartDate", c, true);
                row.SetDataTableRow("warrantyEndDate", c, true);
                row.SetDataTableRow("label", c, false);
                row.SetDataTableRow("isCompactor", c, false);
                row.SetDataTableRow("dimensionUnit", c, false);
                row.SetDataTableRow("width", c, true);
                row.SetDataTableRow("length", c, true);
                row.SetDataTableRow("height", c, true);
                row.SetDataTableRow("materialId", c, false);
                row.SetDataTableRow("RowNumber", c, false);
                table.Rows.Add(row);
            }
            return table;
        }
    }
}

﻿using System;

using System.Runtime.Serialization;


namespace InfraStructure.Models
{
    [DataContract]
    public class Customer 
    {
        [DataMember(Name = "autoId")]
        public int Id { get; set; }

        [DataMember(Name = "Code")]
        public string Code { get; set; }

        [DataMember(Name = "version")]
        public short? Version { get; set; }

        [DataMember(Name = "billingId")]
        public string BillingId { get; set; }
        [DataMember(Name = "class")]
        public string Class { get; set; }
        [DataMember(Name = "classId")]
        public int? ClassId { get; set; }
        [DataMember(Name = "businessName")]
        public string BusinessName { get; set; }
        [DataMember(Name = "lastName")]
        public string LastName { get; set; }
        [DataMember(Name = "officePhone")]
        public string OfficePhone { get; set; }
        [DataMember(Name = "homePhone")]
        public string HomePhone { get; set; }
        [DataMember(Name = "Fax")]
        public string Fax { get; set; }
        [DataMember(Name = "email")]
        public string Email { get; set; }
        [DataMember(Name = "active")]
        public short? Active { get; set; }
        [DataMember(Name = "comments")]
        public string Comments { get; set; }
        [DataMember(Name = "billable")]
        public bool? Billable { get; set; }
        [DataMember(Name = "civicNumber")]
        public string CivicNumber { get; set; }
        [DataMember(Name = "street")]
        public string Street { get; set; }
        [DataMember(Name = "suite")]
        public string Suite { get; set; }
        [DataMember(Name = "district")]
        public string District { get; set; }
        [DataMember(Name = "city")]
        public string City { get; set; }
        [DataMember(Name = "state")]
        public string State { get; set; }
        [DataMember(Name = "postalCode")]
        public string PostalCode { get; set; }
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }
        [DataMember(Name = "prospect")]
        public bool Prospect { get; set; }
        [DataMember(Name = "woidRequired")]
        public bool WoidRequired { get; set; }   //FMRMS-162
        [DataMember(Name = "customerPrfxAutoId")]
        public short? CustomerPrfxAutoId { get; set; }
        [DataMember(Name = "customerPrfx")]
        public string CustomerPrfx { get; set; }
        [DataMember(Name = "createdTs")]
        public DateTime CreatedTs { get; set; }
        [DataMember(Name = "modifiedTs")]
        public DateTime? ModifiedTs { get; set; }
        [DataMember(Name = "suspensionStartTs")]
        public DateTime? SuspensionStartTs { get; set; }
        [DataMember(Name = "suspensionEndTs")]
        public DateTime? SuspensionEndTs { get; set; }
        [DataMember(Name = "modifiedBy")]
        public string ModifiedBy { get; set; }
        [DataMember(Name = "createdBy")]
        public string CreatedBy { get; set; }

        //public virtual ICollection<localization> localizations { get; set; }
        //public virtual ICollection<customerComplaint> customerComplaints { get; set; }
        //public virtual customerClass customerClass { get; set; }
    }
}

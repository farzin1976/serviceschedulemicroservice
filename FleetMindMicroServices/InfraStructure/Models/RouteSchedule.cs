﻿using System;
using System.Collections.Generic;

namespace InfraStructure.Models
{
    public partial class RouteSchedule
    {
        public int AutoId { get; set; }
        public DateTime ScheduledDate { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public int RouteId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedTs { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedTs { get; set; }

        public Route Route { get; set; }
    }
}

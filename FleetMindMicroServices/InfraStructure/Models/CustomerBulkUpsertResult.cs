﻿namespace InfraStructure.Models
{
    public class CustomerBulkUpsertResult
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int RowNumber { get; set; }
    }
}

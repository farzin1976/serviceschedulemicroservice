﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace InfraStructure.Models
{
    public partial class FleetLinkRouteSystemContext : DbContext
    {
        public FleetLinkRouteSystemContext()
        {
        }

        public FleetLinkRouteSystemContext(DbContextOptions<FleetLinkRouteSystemContext> options)
            : base(options)
        {
            Database.SetCommandTimeout(9000);
        }

        public virtual DbSet<Frequency> Frequency { get; set; }
        public virtual DbSet<Route> Route { get; set; }
        public virtual DbSet<RouteSchedule> RouteSchedule { get; set; }
        public virtual DbSet<ServiceSchedule> ServiceSchedule { get; set; }
        public virtual DbSet<ServiceType> ServiceType { get; set; }
        public virtual DbSet<RouteStop> RouteStops { get; set; }
        public virtual DbSet<CustomerBulkUpsertResult> CustomerBulkUpsertResults { get; set; }
        public virtual DbSet<LocationBulkUpsertResult> LocationBulkUpsertResults { get; set; }

        public List<RouteStop> GetRouteStops(int routeId)
        {

            try
            {

                return this.RouteStops.FromSql("EXEC GetRouteStops {0}", routeId).ToListAsync().Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ContainerBulkUpsert> ContainerBulkUpsert(IEnumerable<ContainerDTO> containers)
        {
            var data = ContainerTable.Create(containers);
            string conString = @"Server=localhost;Database=FleetLinkRouteSystem;user=fle;password=password;";//Database.GetDbConnection().ConnectionString;
            using (SqlConnection con = new SqlConnection(conString))
            {
                //Make a temp table in sql server that matches our production table
                string tmpTable = "create table ##Container " +
                        "([serialNumber] [varchar](20) NULL,"+
                        "[version][smallint] NULL,"+
                        "[description] [varchar] (60) NULL,"+
	                    "[electronicKey] [varchar] (64) NULL,"+
                        "[capacity] [float] NULL," +
                        "[unit] [int] NULL," +
                        "[colour] [int] NULL," +
                        "[containerTypeId] [int] NULL," +
                        "[active] [smallint] NULL," +
                        "[damaged] [bit] NULL," +
                        "[comments][varchar](max) NULL," +
                        "[iconName] [varchar] (15) NULL," +
                        "[containerMakerId] [int] NULL," +
                        "[warrantyStartDate] [datetime] NULL," +
                        "[warrantyEndDate] [datetime] NULL," +
                        "[label] [varchar] (32) NULL," +
                        "[isCompactor] [bit] NULL," +
                        "[dimensionUnit] [int] NULL," +
                        "[width] [decimal](10, 2) NULL," +
                        "[length] [decimal](10, 2) NULL," +
                        "[height] [decimal](10, 2) NULL," +
                        "[materialId] [int] NULL," +
                        "[RowNumber] INT)";

                con.Open();


                //Execute the command to make a temp table
                SqlCommand cmd = new SqlCommand(tmpTable, con);
                cmd.ExecuteNonQuery();

                //BulkCopy the data in the DataTable to the temp table
                using (SqlBulkCopy bulk = new SqlBulkCopy(con))
                {
                    try
                    {
                        bulk.DestinationTableName = "##Location";
                        bulk.WriteToServer(data);
                    }
                    catch (SqlException ex)
                    {
                        if (ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                        {
                            string pattern = @"\d+";
                            Match match = Regex.Match(ex.Message.ToString(), pattern);
                            var index = Convert.ToInt32(match.Value) - 1;

                            FieldInfo fi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                            var sortedColumns = fi.GetValue(bulk);
                            var items = (Object[])sortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sortedColumns);

                            FieldInfo itemdata = items[index].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                            var metadata = itemdata.GetValue(items[index]);

                            var column = metadata.GetType().GetField("column", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                            var length = metadata.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                            //throw new DataFormatException(String.Format("Column: {0} contains data with a length greater than: {1}", column, length));
                            throw;
                        }

                        throw;
                    }
                }
                return new List<ContainerBulkUpsert>();
            }

        }
        public List<LocationBulkUpsertResult> LocationBulkUpsert(IEnumerable<LocationDTO> locations)
        {
            var data = LocationTable.Create(locations);
            string conString = @"Server=localhost;Database=FleetLinkRouteSystem;user=fle;password=password;";//Database.GetDbConnection().ConnectionString;
            using (SqlConnection con = new SqlConnection(conString))
            {
                //Make a temp table in sql server that matches our production table
                string tmpTable = "create table ##Location " +
                        "([Code] VARCHAR(64)," +
                        "[CivicNumber] VARCHAR(12)," +
                        "[Street] VARCHAR(512)," +
                        "[Suite] VARCHAR(13)," +
                        "[District] VARCHAR(40)," +
                        "[City] VARCHAR(40)," +
                        "[State] VARCHAR(2)," +
                        "[PostalCode] VARCHAR(12)," +
                        "[BuildingCategoryId] INT," +
                        "[CustomerId] INT," +
                        "[ContactName] VARCHAR(64)," +
                        "[ContactPhone] VARCHAR(25)," +
                        "[Latitude] VARCHAR(40)," +
                        "[Longitude] VARCHAR(40)," +
                        "[LocationName] VARCHAR(512)," +
                        "[GeoZoneId] VARCHAR(36)," +
                        "[User] VARCHAR(36)," +
                        "[RowNumber] INT)";

                con.Open();


                //Execute the command to make a temp table
                SqlCommand cmd = new SqlCommand(tmpTable, con);
                cmd.ExecuteNonQuery();

                //BulkCopy the data in the DataTable to the temp table
                using (SqlBulkCopy bulk = new SqlBulkCopy(con))
                {
                    try
                    {
                        bulk.DestinationTableName = "##Location";
                        bulk.WriteToServer(data);
                    }
                    catch (SqlException ex)
                    {
                        if (ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                        {
                            string pattern = @"\d+";
                            Match match = Regex.Match(ex.Message.ToString(), pattern);
                            var index = Convert.ToInt32(match.Value) - 1;

                            FieldInfo fi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                            var sortedColumns = fi.GetValue(bulk);
                            var items = (Object[])sortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sortedColumns);

                            FieldInfo itemdata = items[index].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                            var metadata = itemdata.GetValue(items[index]);

                            var column = metadata.GetType().GetField("column", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                            var length = metadata.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                            //throw new DataFormatException(String.Format("Column: {0} contains data with a length greater than: {1}", column, length));
                            throw;
                        }

                        throw;
                    }

                }
                var bulkUpsertResults = this.LocationBulkUpsertResults.FromSql(@"EXEC LocationBulkUpsert").ToListAsync().Result;

                //Clean up the temp table
                cmd.CommandText = "DROP TABLE ##Location";
                cmd.ExecuteNonQuery();

                return bulkUpsertResults;
            }

        }
        public List<CustomerBulkUpsertResult> CustomerBulkUpsert(IEnumerable<CustomerDTO> customers)
        {
            var data = CustomerTable.Create(customers);
            string conString = @"Server=localhost;Database=FleetLinkRouteSystem;user=fle;password=password;";//Database.GetDbConnection().ConnectionString;
            using (SqlConnection con = new SqlConnection(conString))
            {

                //Make a temp table in sql server that matches our production table
                string tmpTable = "create table ##Customer " +
                        "([Code] VARCHAR(64)," +
                        "[businessName] VARCHAR(64)," +
                        "[lastName] VARCHAR(64)," +
                        "[firstName] VARCHAR(64)," +
                        "[officePhone] VARCHAR(24)," +
                        "[homePhone] VARCHAR(24)," +
                        "[fax] VARCHAR(24)," +
                        "[email] VARCHAR(64)," +
                        "[billingId] VARCHAR(64)," +
                        "[civicNumber] VARCHAR(12)," +
                        "[street] VARCHAR(64)," +
                        "[suite] VARCHAR(13)," +
                        "[district] VARCHAR(64)," +
                        "[city] VARCHAR(64)," +
                        "[state] VARCHAR(2)," +
                        "[postalCode] VARCHAR(12)," +
                        "[active] SMALLINT," +
                        "[billable] BIT," +
                        "[comments] VARCHAR(256)," +
                        "[classId] SMALLINT," +
                        "[customerPrfxId] SMALLINT," +
                        "[user] VARCHAR(32)," +
                        "[suspensionStartTs] DATETIME," +
                        "[suspensionEndTs] DATETIME," +
                        "[RowNumber] INT)";

                con.Open();
                //Execute the command to make a temp table
                SqlCommand cmd = new SqlCommand(tmpTable, con);
                cmd.ExecuteNonQuery();
                //BulkCopy the data in the DataTable to the temp table
                using (SqlBulkCopy bulk = new SqlBulkCopy(con))
                {
                    try
                    {
                        bulk.DestinationTableName = "##Customer";
                        bulk.WriteToServer(data);
                    }
                    catch (SqlException ex)
                    {
                        if (ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                        {
                            string pattern = @"\d+";
                            Match match = Regex.Match(ex.Message.ToString(), pattern);
                            var index = Convert.ToInt32(match.Value) - 1;

                            FieldInfo fi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                            var sortedColumns = fi.GetValue(bulk);
                            var items = (Object[])sortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sortedColumns);

                            FieldInfo itemdata = items[index].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                            var metadata = itemdata.GetValue(items[index]);

                            var column = metadata.GetType().GetField("column", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                            var length = metadata.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                            //throw new DataFormatException(String.Format("Column: {0} contains data with a length greater than: {1}", column, length));
                            throw;
                        }

                        throw;
                    }
                }

                var bulkUpsertResults = this.CustomerBulkUpsertResults.FromSql("EXEC CustomerBulkUpsert ").ToListAsync().Result;

                //Clean up the temp table
                cmd.CommandText = "DROP TABLE ##Customer";
                cmd.ExecuteNonQuery();

                return bulkUpsertResults;
            }


        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer("Server=localhost;Database=FleetLinkRouteSystem;user=fle;password=password;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Frequency>(entity =>
            {
                entity.HasKey(e => e.AutoId);

                entity.ToTable("frequency");

                entity.HasIndex(e => e.Name)
                    .HasName("frequency_uidx_1")
                    .IsUnique();

                entity.Property(e => e.AutoId).HasColumnName("autoId");

                entity.Property(e => e.BaseFrequencyId).HasColumnName("baseFrequencyId");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTs)
                    .HasColumnName("createdTs")
                    .HasColumnType("datetime");

                entity.Property(e => e.DayOfWeek).HasColumnName("dayOfWeek");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(128);

                entity.Property(e => e.EvenWeeksOnly)
                    .HasColumnName("evenWeeksOnly")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.FrequencyInterval).HasColumnName("frequencyInterval");

                entity.Property(e => e.FrequencyIntervalId).HasColumnName("frequencyIntervalId");

                entity.Property(e => e.Friday)
                    .HasColumnName("friday")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("modifiedBy")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedTs)
                    .HasColumnName("modifiedTs")
                    .HasColumnType("datetime");

                entity.Property(e => e.Monday)
                    .HasColumnName("monday")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MonthOfYear).HasColumnName("monthOfYear");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.OddWeeksOnly)
                    .HasColumnName("oddWeeksOnly")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Saturday)
                    .HasColumnName("saturday")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Sunday)
                    .HasColumnName("sunday")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Thursday)
                    .HasColumnName("thursday")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Tuesday)
                    .HasColumnName("tuesday")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('W')");

                entity.Property(e => e.Wednesday)
                    .HasColumnName("wednesday")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.WeekDay).HasColumnName("weekDay");

                entity.Property(e => e.WeekNumber).HasColumnName("weekNumber");

                entity.Property(e => e.WeekOfMonth).HasColumnName("weekOfMonth");
            });

            modelBuilder.Entity<Route>(entity =>
            {
                entity.HasKey(e => e.AutoId);

                entity.ToTable("route");

                entity.HasIndex(e => e.ContainerTypeId1)
                    .HasName("IX_containerTypeId1");

                entity.HasIndex(e => e.ContainerTypeId2)
                    .HasName("IX_containerTypeId2");

                entity.HasIndex(e => e.ContainerTypeId3)
                    .HasName("IX_containerTypeId3");

                entity.HasIndex(e => e.CustomerTypeId)
                    .HasName("IX_customerTypeId");

                entity.HasIndex(e => e.FrequencyAutoId)
                    .HasName("IX_frequencyAutoId");

                entity.HasIndex(e => e.MaterialAutoId1)
                    .HasName("IX_materialAutoId1");

                entity.HasIndex(e => e.MaterialAutoId2)
                    .HasName("IX_materialAutoId2");

                entity.HasIndex(e => e.MaterialAutoId3)
                    .HasName("IX_materialAutoId3");

                entity.HasIndex(e => e.Name)
                    .HasName("route_uidx")
                    .IsUnique();

                entity.HasIndex(e => e.WorkFlowGroupAutoId)
                    .HasName("IX_WorkFlowGroupAutoId");

                entity.Property(e => e.AutoId).HasColumnName("autoId");

                entity.Property(e => e.ContainerTypeId1).HasColumnName("containerTypeId1");

                entity.Property(e => e.ContainerTypeId2).HasColumnName("containerTypeId2");

                entity.Property(e => e.ContainerTypeId3).HasColumnName("containerTypeId3");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTs)
                    .HasColumnName("createdTs")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustomerTypeId).HasColumnName("customerTypeId");

                entity.Property(e => e.ExpirationDays).HasColumnName("expirationDays");

                entity.Property(e => e.FrequencyAutoId).HasColumnName("frequencyAutoId");

                entity.Property(e => e.IsTemporary).HasColumnName("isTemporary");

                entity.Property(e => e.LastDispatchTs)
                    .HasColumnName("lastDispatchTs")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastModified)
                    .HasColumnName("lastModified")
                    .HasColumnType("datetime");

                entity.Property(e => e.MaterialAutoId1).HasColumnName("materialAutoId1");

                entity.Property(e => e.MaterialAutoId2).HasColumnName("materialAutoId2");

                entity.Property(e => e.MaterialAutoId3).HasColumnName("materialAutoId3");

                entity.Property(e => e.Message)
                    .HasColumnName("message")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("modifiedBy")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedTs)
                    .HasColumnName("modifiedTs")
                    .HasColumnType("datetime");

                entity.Property(e => e.MultiDayRoute).HasColumnName("multiDayRoute");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.OperatorAutoId).HasColumnName("operatorAutoId");

                entity.Property(e => e.PermTruckAutoId).HasColumnName("permTruckAutoId");

                entity.Property(e => e.ServiceDate)
                    .HasColumnName("serviceDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TmpTruckAutoId).HasColumnName("tmpTruckAutoId");

                entity.Property(e => e.TmpTruckEndDate)
                    .HasColumnName("tmpTruckEndDate")
                    .HasColumnType("date");

                entity.Property(e => e.TmpTruckStartDate)
                    .HasColumnName("tmpTruckStartDate")
                    .HasColumnType("date");

                entity.Property(e => e.TruckAutoId).HasColumnName("truckAutoId");

                entity.Property(e => e.UpdateGeoLocation).HasColumnName("updateGeoLocation");

                entity.Property(e => e.UpdateRfid).HasColumnName("updateRfid");

                entity.Property(e => e.UpdateSerNbr).HasColumnName("updateSerNbr");

                entity.Property(e => e.WorkFlowGroupAutoId).HasColumnName("workFlowGroupAutoId");

                entity.Property(e => e.WorkGroupAutoId).HasColumnName("workGroupAutoId");

                entity.HasOne(d => d.FrequencyAuto)
                    .WithMany(p => p.Route)
                    .HasForeignKey(d => d.FrequencyAutoId)
                    .HasConstraintName("FK_dbo.route_dbo.frequency_frequencyAutoId");
            });

            modelBuilder.Entity<RouteSchedule>(entity =>
            {
                entity.HasKey(e => e.AutoId);

                entity.ToTable("routeSchedule");

                entity.HasIndex(e => e.RouteId)
                    .HasName("IX_routeId");

                entity.HasIndex(e => new { e.Status, e.RouteId })
                    .HasName("IX_RouteIdStatus");

                entity.Property(e => e.AutoId).HasColumnName("autoId");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTs)
                    .HasColumnName("createdTs")
                    .HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("modifiedBy")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedTs)
                    .HasColumnName("modifiedTs")
                    .HasColumnType("datetime");

                entity.Property(e => e.RouteId).HasColumnName("routeId");

                entity.Property(e => e.ScheduledDate)
                    .HasColumnName("scheduledDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.HasOne(d => d.Route)
                    .WithMany(p => p.RouteSchedule)
                    .HasForeignKey(d => d.RouteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.routeSchedule_dbo.route_routeId");
            });

            modelBuilder.Entity<ServiceSchedule>(entity =>
            {
                entity.HasKey(e => e.AutoId);

                entity.ToTable("serviceSchedule");

                entity.HasIndex(e => e.AssignmentId)
                    .HasName("IX_assignmentId");

                entity.HasIndex(e => e.ServiceId)
                    .HasName("IX_serviceId");

                entity.Property(e => e.AutoId).HasColumnName("autoId");

                entity.Property(e => e.AssignmentId).HasColumnName("assignmentId");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTs)
                    .HasColumnName("createdTs")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("modifiedBy")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedTs)
                    .HasColumnName("modifiedTs")
                    .HasColumnType("datetime");

                entity.Property(e => e.ScheduleStatus).HasColumnName("scheduleStatus");

                entity.Property(e => e.ScheduledDate)
                    .HasColumnName("scheduledDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ServiceDate)
                    .HasColumnName("serviceDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ServiceId).HasColumnName("serviceId");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.ServiceSchedule)
                    .HasForeignKey(d => d.ServiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.serviceSchedule_dbo.serviceType_serviceId");
            });

            modelBuilder.Entity<ServiceType>(entity =>
            {
                entity.HasKey(e => e.AutoId);

                entity.ToTable("serviceType");

                entity.HasIndex(e => e.CustomerTypeAutoId)
                    .HasName("IX_customerTypeAutoId");

                entity.HasIndex(e => e.DefaultWorkFlowAutoId)
                    .HasName("IX_defaultWorkFlowAutoId");

                entity.HasIndex(e => e.DispatchTypeAutoId)
                    .HasName("IX_dispatchTypeAutoId");

                entity.HasIndex(e => e.MaterialTypeId)
                    .HasName("IX_materialTypeId");

                entity.HasIndex(e => e.ServiceFrequencyId)
                    .HasName("IX_serviceFrequencyId");

                entity.HasIndex(e => e.ServiceRateFrequencyAutoId)
                    .HasName("IX_serviceRateFrequencyAutoId");

                entity.HasIndex(e => e.WeightUnitId)
                    .HasName("IX_weightUnitId");

                entity.HasIndex(e => new { e.Name, e.DispatchTypeAutoId })
                    .HasName("serviceType_uidx_1")
                    .IsUnique();

                entity.Property(e => e.AutoId).HasColumnName("autoId");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTs)
                    .HasColumnName("createdTs")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustomerTypeAutoId).HasColumnName("customerTypeAutoId");

                entity.Property(e => e.DefaultWorkFlowAutoId).HasColumnName("defaultWorkFlowAutoId");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DispatchTypeAutoId).HasColumnName("dispatchTypeAutoId");

                entity.Property(e => e.Extra).HasColumnName("extra");

                entity.Property(e => e.IsAlley).HasColumnName("isAlley");

                entity.Property(e => e.MaterialTypeId).HasColumnName("materialTypeId");

                entity.Property(e => e.MinWeight)
                    .HasColumnName("minWeight")
                    .HasColumnType("decimal(10, 3)");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("modifiedBy")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedTs)
                    .HasColumnName("modifiedTs")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.OnDemand).HasColumnName("onDemand");

                entity.Property(e => e.Recurring).HasColumnName("recurring");

                entity.Property(e => e.Sequence).HasColumnName("sequence");

                entity.Property(e => e.ServiceFrequencyId).HasColumnName("serviceFrequencyId");

                entity.Property(e => e.ServiceRateFrequencyAutoId).HasColumnName("serviceRateFrequencyAutoId");

                entity.Property(e => e.Weight).HasColumnName("weight");

                entity.Property(e => e.WeightUnitId).HasColumnName("weightUnitId");
            });

            modelBuilder.Entity<RouteStop>(entity =>
            {
                entity.HasKey(e => e.StopId);


            });
        }
    }
}

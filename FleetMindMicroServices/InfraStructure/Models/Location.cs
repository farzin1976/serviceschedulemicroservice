﻿using System;
using System.Runtime.Serialization;




namespace InfraStructure.Models
{
    [DataContract]
    public partial class Location 
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "assignmentId")]
        public int AssignmentId { get; set; }

        [DataMember(Name = "civicNumber")]
        public string CivicNumber { get; set; }

        [DataMember(Name = "street")]
        public string Street { get; set; }

        [DataMember(Name = "suite")]
        public string Suite { get; set; }

        [DataMember(Name = "district")]
        public string District { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "state")]
        public string StateCode { get; set; }

        [DataMember(Name = "postalCode")]
        public string PostalCode { get; set; }

        [DataMember(Name = "buildingCategory")]
        public string BuildingCategory { get; set; }

        [DataMember(Name = "buildingCategoryId")]
        public int? BuildingCategoryId { get; set; }

        [DataMember(Name = "customerId")]
        public int? CustomerId { get; set; }

        [DataMember(Name = "contactName")]
        public string ContactName { get; set; }

        [DataMember(Name = "contactPhone")]
        public string ContactPhone { get; set; }

        [DataMember(Name = "longitude")]
        public string Longitude { get; set; }

        [DataMember(Name = "latitude")]
        public string Latitude { get; set; }

        [DataMember(Name = "locationName")]
        public string LocationName { get; set; }



        [DataMember(Name = "active")]
        public bool Active { get; set; }

        [DataMember(Name = "prospect")]
        public bool Prospect { get; set; }

        [DataMember(Name = "geoId")]
        public string GeoId { get; set; }

        [DataMember(Name = "isStreetZone")]
        public bool IsStreetZone { get; set; }

        [DataMember(Name = "suspensionStartTs")]
        public DateTime? SuspensionStartTs { get; set; }

        [DataMember(Name = "suspensionEndTs")]
        public DateTime? SuspensionEndTs { get; set; }

        [DataMember(Name = "createdBy")]
        public string CreatedBy { get; set; }

        [DataMember(Name = "createdTs")]
        public DateTime CreatedTs { get; set; }

        [DataMember(Name = "modifiedBy")]
        public string ModifiedBy { get; set; }

        [DataMember(Name = "modifiedTs")]
        public DateTime? ModifiedTs { get; set; }

    }
}

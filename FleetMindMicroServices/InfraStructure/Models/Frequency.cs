﻿using System;
using System.Collections.Generic;

namespace InfraStructure.Models
{
    public partial class Frequency
    {
        public Frequency()
        {
            Route = new HashSet<Route>();
        }

        public int AutoId { get; set; }
        public string Name { get; set; }
        public short? Sunday { get; set; }
        public short? Monday { get; set; }
        public short? Tuesday { get; set; }
        public short? Wednesday { get; set; }
        public short? Thursday { get; set; }
        public short? Friday { get; set; }
        public short? Saturday { get; set; }
        public short? EvenWeeksOnly { get; set; }
        public short? OddWeeksOnly { get; set; }
        public string Type { get; set; }
        public int? WeekNumber { get; set; }
        public int? WeekDay { get; set; }
        public DateTime CreatedTs { get; set; }
        public DateTime? ModifiedTs { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public int? BaseFrequencyId { get; set; }
        public int FrequencyInterval { get; set; }
        public int? MonthOfYear { get; set; }
        public int? WeekOfMonth { get; set; }
        public int? DayOfWeek { get; set; }
        public int? FrequencyIntervalId { get; set; }
        public string Description { get; set; }

        public ICollection<Route> Route { get; set; }
    }
}
